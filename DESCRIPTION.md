## Overview

Mealie is a recipe manager and meal planner for the whole family.
Easily add recipes into your database by providing the url and Mealie will automatically import the relevant data or add a family recipe with the UI editor.
