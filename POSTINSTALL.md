This app is pre-setup with an admin account. The initial credentials are:

**Username**: changeme@example.com<br/>
**Password**: MyPassword<br/>

Please change the admin password immediately.
