#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.EMAIL || !process.env.PASSWORD) {
    console.log('EMAIL, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    var EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    var TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    var browser, app, shareLink;

    var ADMIN_EMAIL = 'changeme@example.com'
    var ADMIN_EMAIL_NEW = 'changemeagain@example.com'
    var ADMIN_USERNAME = 'myadmin'
    var ADMIN_PASSWORD = 'MyPassword'
    var ADMIN_NAME = 'Ad Min'

    var username = process.env.EMAIL;
    var password = process.env.PASSWORD;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1700, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function setup(email, password, username, name, newEmail) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);
        await browser.sleep(2000);

        await waitForElement(By.xpath('//input[@name="login"]'));
        await browser.findElement(By.xpath('//input[@name="login"]')).sendKeys(email);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[@type="submit" and contains(., "Login")]')).click();

        // account setup flow
        await waitForElement(By.xpath('//button[contains(.,"Start")]'));
        await browser.findElement(By.xpath('//button[contains(.,"Start")]')).click();

        await waitForElement(By.xpath('//label[contains(text(),"Username")]/following-sibling::input'));
        await browser.findElement(By.xpath('//label[contains(text(),"Username")]/following-sibling::input')).sendKeys(username);

        await waitForElement(By.xpath('//label[contains(text(),"Full Name")]/following-sibling::input'));
        await browser.findElement(By.xpath('//label[contains(text(),"Full Name")]/following-sibling::input')).sendKeys(name);

        await waitForElement(By.xpath('//label[contains(text(),"Email")]/following-sibling::input'));
        await browser.findElement(By.xpath('//label[contains(text(),"Email")]/following-sibling::input')).sendKeys(newEmail);

        await waitForElement(By.xpath('//label[contains(text(),"Password")]/following-sibling::input'));
        await browser.findElement(By.xpath('//label[contains(text(),"Password")]/following-sibling::input')).sendKeys(password);

        await waitForElement(By.xpath('//label[contains(text(),"Confirm Password")]/following-sibling::input'));
        await browser.findElement(By.xpath('//label[contains(text(),"Confirm Password")]/following-sibling::input')).sendKeys(password);

        await waitForElement(By.xpath('//button[contains(.,"Next")]'));
        await browser.findElement(By.xpath('//button[contains(.,"Next")]')).click();

        await browser.sleep(2000);

        await waitForElement(By.xpath('//button[contains(.,"Next")]'));
        await browser.findElement(By.xpath('//button[contains(.,"Next")]')).click();

        await browser.sleep(2000);

        await waitForElement(By.xpath('//button[contains(.,"Submit")]'));
        await browser.findElement(By.xpath('//button[contains(.,"Submit")]')).click();

        await browser.sleep(2000);

        await waitForElement(By.xpath('//button[contains(.,"Home")]'));
        await browser.findElement(By.xpath('//button[contains(.,"Home")]')).click();

        await waitForElement(By.xpath('//div[.="Recipes"]'));
    }

    async function login(username, password) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);
        await browser.sleep(2000);

        await waitForElement(By.xpath('//input[@name="login"]'));
        await browser.findElement(By.xpath('//input[@name="login"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[@type="submit" and contains(., "Login")]')).click();

        await waitForElement(By.xpath('//div[.="Recipes"]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/');
        await browser.sleep(5000);
        await browser.findElement(By.xpath('//button[contains(., "Logout")]')).click();
        await browser.sleep(3000);
        await waitForElement(By.xpath('//input[@name="login"]'));
    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn + '/');
        await waitForElement(By.xpath('//div[.="Recipes"]'));
    }

    async function createRecipe(recipename) {
        await browser.get(`https://${app.fqdn}/recipe/create/new`);
        await browser.sleep(5000);

        await browser.findElement(By.xpath('//label[contains(text(), "Recipe Name")]/following::input[@type="text"]')).sendKeys(recipename);
        await browser.sleep(5000);
        await browser.findElement(By.xpath('//span[@class="v-btn__content"]/ancestor::button[@type="button" and contains(@class, "success")]')).click();
        await browser.sleep(5000);
        await browser.findElement(By.xpath('//button[@type="button" and contains(@class, "success") and contains(., "Save")]')).click();
        await browser.sleep(5000);
        await waitForElement(By.xpath('//div[contains(@class, "headline") and contains(., "' + recipename + '")]'));
    }

    async function checkRecipe(recipename) {
        await browser.get('https://' + app.fqdn + '/');
        await browser.sleep(5000);
        await waitForElement(By.xpath('//div[contains(@class, "headerClass") and contains(text(), "' + recipename + '")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no SSO
    it('install app (no SSO)', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can setup', setup.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD, ADMIN_USERNAME, ADMIN_NAME, ADMIN_EMAIL_NEW));
    it('can get the main page', getMainPage);

    it('can create Recipe', createRecipe.bind(null, 'Test'));
    it('can create Recipe', createRecipe.bind(null, 'Test2'));
    it('check Recipe Test', checkRecipe.bind(null, 'Test'));
    it('check Recipe Test2', checkRecipe.bind(null, 'Test2'));
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // SSO
    it('install app', async function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can setup', setup.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD, ADMIN_USERNAME, ADMIN_NAME, ADMIN_EMAIL_NEW));
    it('can get the main page', getMainPage);

    it('can create Recipe', createRecipe.bind(null, 'Test'));
    it('can create Recipe', createRecipe.bind(null, 'Test2'));
    it('check Recipe Test', checkRecipe.bind(null, 'Test'));
    it('check Recipe Test2', checkRecipe.bind(null, 'Test2'));

    it('can logout', logout);

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await sleep(20000);
    });

    it('can LDAP login', login.bind(null, username, password));
    it('can get the main page', getMainPage);

    it('check Recipe Test', checkRecipe.bind(null, 'Test'));
    it('check Recipe Test2', checkRecipe.bind(null, 'Test2'));
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });

    it('can get app information', getAppInfo);

    it('can LDAP login', login.bind(null, username, password));
    it('can get the main page', getMainPage);

    it('check Recipe Test', checkRecipe.bind(null, 'Test'));
    it('check Recipe Test2', checkRecipe.bind(null, 'Test2'));
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });
    it('can get app information', getAppInfo);

    it('can LDAP login', login.bind(null, username, password));
    it('can get the main page', getMainPage);
    it('check Recipe Test', checkRecipe.bind(null, 'Test'));
    it('check Recipe Test2', checkRecipe.bind(null, 'Test2'));
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app from App Store', async function () {
        execSync(`cloudron install --appstore-id io.mealie.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });
    
    it('can get app information', getAppInfo);
    it('can setup', setup.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD, ADMIN_USERNAME, ADMIN_NAME, ADMIN_EMAIL_NEW));
    it('can get the main page', getMainPage);

    it('can create Recipe', createRecipe.bind(null, 'Test'));
    it('can create Recipe', createRecipe.bind(null, 'Test2'));
    it('check Recipe Test', checkRecipe.bind(null, 'Test'));
    it('check Recipe Test2', checkRecipe.bind(null, 'Test2'));

    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('check Recipe Test', checkRecipe.bind(null, 'Test'));
    it('check Recipe Test2', checkRecipe.bind(null, 'Test2'));
    it('can logout', logout);

    it('can LDAP login', login.bind(null, username, password));
    it('check Recipe Test', checkRecipe.bind(null, 'Test'));
    it('check Recipe Test2', checkRecipe.bind(null, 'Test2'));
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

});

