[0.1.0]
* Initial version for Mealie v1.0.0

[0.1.1]
* Add forum url 

[0.3.0]
* Update Mealie to 1.0.0
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.0.0)

[1.0.0]
* First stable package release
* Update Mealie to 1.1.0
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.1.0)
* feat: Add Ingredient Sections To Copied Ingredients by @michael-genson in #3032
* feat: Edit existing mealplan meals by @JackBailey in #2717
* feat: Improvements To Adding A Recipe To A Shopping List by @michael-genson in #3036
* feat: Add Additional SMTP Headers to Decrease Spam Score by @michael-genson in #3031
* fix: running the container with PUID=0 and PGID=0 by @xThaid in #3030

[1.0.1]
* Update Mealie to 1.1.1
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.1.1)
* fix: Upgrade Black by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3057
* New Crowdin updates by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3059
* chore(deps): update dependency black to v24.1.1 by @renovate in https://github.com/mealie-recipes/mealie/pull/3062
* docs: Change org to mealie-recipes by @boc-the-git in https://github.com/mealie-recipes/mealie/pull/3064
* docs: Update example docker-compose files by @boc-the-git in https://github.com/mealie-recipes/mealie/pull/3065
* chore(deps): update dependency pytest-asyncio to v0.23.4 by @renovate in https://github.com/mealie-recipes/mealie/pull/3069
* New Crowdin updates by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3071
* fix(deps): update dependency recipe-scrapers to v14.54.0 by @renovate in https://github.com/mealie-recipes/mealie/pull/3070
* chore(deps): update dependency mkdocs-material to v9.5.6 by @renovate in https://github.com/mealie-recipes/mealie/pull/3063
* fix(deps): update dependency apprise to v1.7.2 by @renovate in https://github.com/mealie-recipes/mealie/pull/3058
* New Crowdin updates by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3075
* feat: On new release publish, update image tags in sample docker-compose files by @boc-the-git in https://github.com/mealie-recipes/mealie/pull/3072
* fix: recipeOrganizerPage edit dialog label localization by @Kuchenpirat in https://github.com/mealie-recipes/mealie/pull/3079
* feat: Add paho-mqtt package, as required by Apprise to send MQTT messages by @boc-the-git in https://github.com/mealie-recipes/mealie/pull/3078
* chore(deps): update dependency ruff to v0.1.15 by @renovate in https://github.com/mealie-recipes/mealie/pull/3076
* fix: Update Group Slug When Updating Group by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3084
* New Crowdin updates by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3086
* chore: update build link to org by @Kuchenpirat in https://github.com/mealie-recipes/mealie/pull/3088
* cleanup: delete unused user profile page by @Kuchenpirat in https://github.com/mealie-recipes/mealie/pull/3089
* fix: Migration Issue With Duplicate Labels by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3085
* fix(deps): update dependency python-slugify to v8.0.3 by @renovate in https://github.com/mealie-recipes/mealie/pull/3090
* Rectify email message ID, change multipart order by @be-joeri in https://github.com/mealie-recipes/mealie/pull/3094
* New Crowdin updates by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3096
* chore(deps): update dependency ruff to ^0.2.0 by @renovate in https://github.com/mealie-recipes/mealie/pull/3097

[1.1.0]
* Update Mealie to 1.2.0
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.2.0)
* New Crowdin updates by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3103
* chore(deps): update dependency mkdocs-material to v9.5.7 by @renovate in https://github.com/mealie-recipes/mealie/pull/3105
* fix(deps): update dependency fastapi to v0.109.1 by @renovate in https://github.com/mealie-recipes/mealie/pull/3107
* fix(deps): update dependency python-multipart to ^0.0.7 by @renovate in https://github.com/mealie-recipes/mealie/pull/3106
* New Crowdin updates by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3110
* feat: Cookbook Create & Delete Improvements by @Kuchenpirat in https://github.com/mealie-recipes/mealie/pull/2902
* feat: Bulk deletion on "Manage Data" page by @Kuchenpirat in https://github.com/mealie-recipes/mealie/pull/3056
* fix(deps): update dependency fastapi to v0.109.2 by @renovate in https://github.com/mealie-recipes/mealie/pull/3115
* New Crowdin updates by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3116
* fix(deps): update dependency orjson to v3.9.13 by @renovate in https://github.com/mealie-recipes/mealie/pull/3111
* fix: manage data pages not loading by @Kuchenpirat in https://github.com/mealie-recipes/mealie/pull/3120
* chore(deps): update dependency ruff to v0.2.1 by @renovate in https://github.com/mealie-recipes/mealie/pull/3122
* New Crowdin updates by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3123
* chore(deps): update dependency mkdocs-material to v9.5.8 by @renovate in https://github.com/mealie-recipes/mealie/pull/3124
* Better bruteforce parsing for units by @RealFoxie in https://github.com/mealie-recipes/mealie/pull/3066
* feat: Redirect Logged Out Users to Default Group, If It's Public by @michael-genson in https://github.com/mealie-recipes/mealie/pull/2772
* New Crowdin updates by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3130
* fix: Translate ISO 8601 Datetime Durations During Scraping/Parsing/Migrating by @michael-genson in https://github.com/mealie-recipes/mealie/pull/2810
* New Crowdin updates by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3132
* Update dependency python-slugify to v8.0.4 by @renovate in https://github.com/mealie-recipes/mealie/pull/3131
* dev: add docker-in-docker to dev container by @Kuchenpirat in https://github.com/mealie-recipes/mealie/pull/3135
* Update dependency python-multipart to ^0.0.8 by @renovate in https://github.com/mealie-recipes/mealie/pull/3137
* fix: Give update-image-tags job write permissions to the repo, for auto doco updater by @boc-the-git in https://github.com/mealie-recipes/mealie/pull/3138

[1.2.0]
* Update Mealie to 1.3.0
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.3.0)
* Shopping List Editor quality of life improvements. (by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3178)
* The PWA now has shortcuts to the Meal Planner and Shopping Lists. You might need to delete and re-install the PWA to get these changes. (by @boc-the-git in https://github.com/mealie-recipes/mealie/pull/3255)
* Shopping Lists now have owners and by default you'll only see you own shopping lists. (by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3213)
* The ingredients parser now allows dragging and dropping items. (by @Kuchenpirat in https://github.com/mealie-recipes/mealie/pull/3162)

[1.2.1]
* Update Mealie to 1.3.1
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.3.1)
* fix: Invalid Pydantic Definition On Group Model by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3264
* New Crowdin updates by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3262

[1.2.2]
* Update Mealie to 1.3.2
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.3.2)
* docs(auto): Update image tag, for release v1.3.1 by @github-actions in https://github.com/mealie-recipes/mealie/pull/3268
* New Crowdin updates by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3269
* chore(deps): update dependency mypy to v1.9.0 by @renovate in https://github.com/mealie-recipes/mealie/pull/3270
* feat: Change buttons labelled "New"/"Create" to "Add" by @boc-the-git in https://github.com/mealie-recipes/mealie/pull/3271
* fix(deps): update dependency uvicorn to ^0.28.0 by @renovate in https://github.com/mealie-recipes/mealie/pull/3273
* chore(deps): update dependency pytest to v8.1.1 by @renovate in https://github.com/mealie-recipes/mealie/pull/3244
* fix: Make Meal Planner Notes Not Clickable by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3274
* fix: bump ruff by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3275
* fix(deps): update dependency apprise to v1.7.4 by @renovate in https://github.com/mealie-recipes/mealie/pull/3276
* New Crowdin updates by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3277
* fix: sync locales in user registration validation by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3278

[1.3.0]
* Update Mealie to 1.4.0
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.4.0)
* Security updates (more on that below)
* Initial Startup Workflow - https://github.com/mealie-recipes/mealie/pull/3204

[1.4.0]
* Update Mealie to 1.5.0
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.5.0)
* feat: User-specific Recipe Ratings by @michael-genson in #3345
* feat: Add `OIDC_USER_CLAIM` by @tba-code in #3422
* feat: Support HEIF, HEIC and AVIF recipe image uploads by @tba-code in #3409
* feat: rewrite logger to support custom config files by @hay-kot in #3104

[1.4.1]
* Update Mealie to 1.5.1
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.5.1)
* fix: make groups private by default by @p0lycarpio in https://github.com/mealie-recipes/mealie/pull/3474
* fix: Bad Recipe Rating Calc Preventing App Startup by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3475
* fix(deps): update dependency gunicorn to v22 by @renovate in https://github.com/mealie-recipes/mealie/pull/3479

[1.4.2]
* Update Mealie to 1.6.0
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.6.0)
* Quality of Life improvements for the shopping list and search

[1.5.0]
* Update Mealie to 1.7.0
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.7.0)
* feat: Default To Fractions When Unit Is Empty by @michael-genson in #3587
* feat: OpenAI Ingredient Parsing by @michael-genson in #3581
* feat: Data Management from Shopping List by @michael-genson in #3603

[1.6.0]
* Update Mealie to 1.8.0
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.8.0)
* You can now set the default number of days to appear when you load the meal planner; configure it via the calendar/date picker on the meal planner screen.
* New migration option supported, Recipe Keeper
* feat: Set default number of days on meal planner by @boc-the-git in #3650
* feat: Add recipekeeper migration by @derpferd in #3642
* feat: Update Shopping List Timestamp on List Item Update by @michael-genson in #3453
* feat: Handle Safari-mangled backup ZIPs and improve backup UI by @michael-genson in #3674

[1.7.0]
* Email display name support

[1.7.1]
* Update Mealie to 1.9.0
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.9.0)

[1.8.0]
* Update Mealie to 1.10.0
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.10.0)
* Shopping lists got a lot of love in this release
* feat: Insert instruction step above or below. #3731 by @derpferd in #3732
* feat: bulk assign label to foods by @Kuchenpirat in #3750
* feat: If only 1 shopping list, navigate straight to it (Shopping List QoL) by @boc-the-git in #3764
* feat: Add Additional Plan To Eat Columns To Import by @michael-genson in #3776
* feat: Prevent Shopping Lists From Rendering If Redirecting by @michael-genson in #3768
* feat: Add Alerts for Ingredient Parsing Errors by @michael-genson in #3795
* feat: Added serving size to print view by @thomaspijper in #3796
* feat: Make OpenAI Request Timeout Configurable by @michael-genson in #3808
* feat: check all in shopping list view by @ollywelch in #3786
* feat: Auto-label new shopping list items by @michael-genson in #3800

[1.8.1]
* Update Mealie to 1.10.1
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.10.1)

[1.8.2]
* Update Mealie to 1.10.2
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.10.2)

[1.9.0]
* Update Mealie to 1.11.0
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.11.0)
* feat: Show recipe tags on mobile view and meal plan by @AverageMarcus in https://github.com/mealie-recipes/mealie/pull/3864
* feat: Push On Hand Items to Bottom Of Add To Shopping List Dialog by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3862
* feat: Internationalize sent emails by @p0lycarpio in https://github.com/mealie-recipes/mealie/pull/3818
* feat: PWA Additions by @Choromanski in https://github.com/mealie-recipes/mealie/pull/3896
* fix: Minor Typo by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3855
* fix: Restore Webhook Test Functionality by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3857
* fix: Make Mealie Timezone-Aware by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3847
* fix: Add a default value of list when a user's group is None by @cmintey in https://github.com/mealie-recipes/mealie/pull/3872
* fix: Follow redirects during scraping by @zeskeertwee in https://github.com/mealie-recipes/mealie/pull/3875
* fix: Bump other version numbers in GH workflow by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3840
* fix: Use env variable to get alembic config file in exporter by @litchipi in https://github.com/mealie-recipes/mealie/pull/3882
* fix: Create directory used for Docker Secrets by @boc-the-git in https://github.com/mealie-recipes/mealie/pull/3888
* fix: Convert Daily Schedule Time to UTC by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3914
* fix: task py:migrate description by @Kuchenpirat in https://github.com/mealie-recipes/mealie/pull/3932
* fix: prevent postgres credentials leak by @hay-kot in https://github.com/mealie-recipes/mealie/pull/3895
* fix: Offline Shopping List Fixes V2 - Electric Boogaloo by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3837
* fix: Homepage icon and text update by @tyme-dev in https://github.com/mealie-recipes/mealie/pull/3922
* fix: Reduce search tolerance on organizers page by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3950

[1.10.0]
* Update Mealie to 1.12.0
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v1.12.0)
* Import recipes via an image, using OpenAI. https://github.com/mealie-recipes/mealie/pull/3974
* feat: Added fr-BE by @TheSuperBeaver in https://github.com/mealie-recipes/mealie/pull/4004
* feat: If there's only one shopping list, navigate directly to it by @boc-the-git in https://github.com/mealie-recipes/mealie/pull/3958
* feat: Import + Translate recipe images with OpenAI by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3974
* feat: Seed support for plural units by @Choromanski in https://github.com/mealie-recipes/mealie/pull/3933
* fix: recipe clean_time function missing translator argument on recursion by @Kuchenpirat in https://github.com/mealie-recipes/mealie/pull/3969
* fix: Make recipe scraper cleaner more fault tolerant by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3967
* fix: Bump tzdata 2024.1 by @michael-genson in https://github.com/mealie-recipes/mealie/pull/3993
* fix: Don't load from secrets dir if nonexistent or inaccessible by @anoadragon453 in https://github.com/mealie-recipes/mealie/pull/4002

[1.11.0]
* Update Mealie to 2.0.0
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v2.0.0)

[1.12.0]
* Update Mealie to 2.1.0
* [Full changelog](https://github.com/mealie-recipes/mealie/releases/tag/v2.1.0)
* Lots of minor fixes and QoL improvements
* Recipe instructions steps can now be renamed, e.g. instead of "Step 1" you could name it "Make Sauce". Just click on the default title when in edit mode and enter your new title; clear the custom value to revert to default

[1.13.0]
* Update mealie to 2.2.0
* [Full Changelog](https://github.com/mealie-recipes/mealie/releases/tag/v2.2.0)
* feat: implement the possibility to add tls [@&#8203;wim-de-groot](https://github.com/wim-de-groot) ([#&#8203;4456](https://github.com/mealie-recipes/mealie/issues/4456))
* feat: Show Cookbooks from Other Households [@&#8203;michael-genson](https://github.com/michael-genson) ([#&#8203;4452](https://github.com/mealie-recipes/mealie/issues/4452))
* feat: adds descriptions to feature checks and add them to logs [@&#8203;cmintey](https://github.com/cmintey) ([#&#8203;4504](https://github.com/mealie-recipes/mealie/issues/4504))
* feat: OIDC: add the ability to override the requested scopes [@&#8203;cmintey](https://github.com/cmintey) ([#&#8203;4530](https://github.com/mealie-recipes/mealie/issues/4530))
* fix: set useFractions on Unit creation to true by default [@&#8203;Kuchenpirat](https://github.com/Kuchenpirat) ([#&#8203;4469](https://github.com/mealie-recipes/mealie/issues/4469))
* fix: round ingredient amounts when not using fractions [@&#8203;Kuchenpirat](https://github.com/Kuchenpirat) ([#&#8203;4470](https://github.com/mealie-recipes/mealie/issues/4470))
* fix: Added Nutrients Suffix to the PrintView and some formatting to that [@&#8203;shethshlok](https://github.com/shethshlok) ([#&#8203;4493](https://github.com/mealie-

[1.14.0]
* Update mealie to 2.3.0
* [Full Changelog](https://github.com/mealie-recipes/mealie/releases/tag/v2.3.0)
* Cook Mode has had an overhaul and is now available even if you've not linked ingredients to your steps. Thanks [@&#8203;codetakki](https://github.com/codetakki)
* Admins can now generate invite links for a specific household. Thanks [@&#8203;p0lycarpio](https://github.com/p0lycarpio)
* feat: Added a dedicated cookmode dialog that allows for individual scrolling [@&#8203;codetakki](https://github.com/codetakki) ([#&#8203;4464](https://github.com/mealie-recipes/mealie/issues/4464))
* feat: Add Ingredients to Recipe Query Filter options [@&#8203;alexxxxxxxandria](https://github.com/alexxxxxxxandria) ([#&#8203;4534](https://github.com/mealie-recipes/mealie/issues/4534))
* feat: Groups/households custom invitations [@&#8203;p0lycarpio](https://github.com/p0lycarpio) ([#&#8203;4252](https://github.com/mealie-recipes/mealie/issues/4252))
* feat: Improve Recipe Imports with Cleaner [@&#8203;michael-genson](https://github.com/michael-genson) ([#&#8203;4517](https://github.com/mealie-recipes/mealie/issues/4517))
* feat: Structured Yields [@&#8203;michael-genson](https://github.com/michael-genson) ([#&#8203;4489](https://github.com/mealie-recipes/mealie/issues/4489))

[1.15.0]
* Update mealie to 2.4.0
* [Full Changelog](https://github.com/mealie-recipes/mealie/releases/tag/v2.4.0)
* Michael upgraded the version of Python we use, from 3.10 to 3.12 in [#&#8203;4675](https://github.com/mealie-recipes/mealie/issues/4675)
* feat: Recipe Finder (aka Cocktail Builder) [@&#8203;michael-genson](https://github.com/michael-genson) ([#&#8203;4542](https://github.com/mealie-recipes/mealie/issues/4542))
* feat: Move alembic config into mealie package for easier distribution [@&#8203;chishm](https://github.com/chishm) ([#&#8203;4329](https://github.com/mealie-recipes/mealie/issues/4329))
* feat: Upgrade to Python 3.12 [@&#8203;michael-genson](https://github.com/michael-genson) ([#&#8203;4675](https://github.com/mealie-recipes/mealie/issues/4675))
* feat: Add 'No Shopping Lists Found' message [@&#8203;niteflyunicorns](https://github.com/niteflyunicorns) ([#&#8203;4661](https://github.com/mealie-recipes/mealie/issues/4661))

[1.15.1]
* Update mealie to 2.4.1
* [Full Changelog](https://github.com/mealie-recipes/mealie/releases/tag/v2.4.1)
* fix: Use configured server time when calling RepositoryMeals.get_today() method  [@&#8203;michaelclark2](https://github.com/michaelclark2) ([#&#8203;4734](https://github.com/mealie-recipes/mealie/issues/4734))
* chore(auto): Update pre-commit hooks [@&#8203;github-actions](https://github.com/github-actions) ([#&#8203;4732](https://github.com/mealie-recipes/mealie/issues/4732))
* chore(l10n): New Crowdin updates [@&#8203;hay-kot](https://github.com/hay-kot) ([#&#8203;4743](https://github.com/mealie-recipes/mealie/issues/4743))
* docs(auto): Update image tag, for release v2.4.0 [@&#8203;github-actions](https://github.com/github-actions) ([#&#8203;4722](https://github.com/mealie-recipes/mealie/issues/4722))
* fix(deps): update dependency uvicorn to ^0.33.0 [@&#8203;renovate](https://github.com/renovate) ([#&#8203;4724](https://github.com/mealie-recipes/mealie/issues/4724))
* fix(deps): update dependency recipe-scrapers to v15.3.3 [@&#8203;renovate](https://github.com/renovate) ([#&#8203;4725](https://github.com/mealie-recipes/mealie/issues/4725))
* chore(deps): update dependency mkdocs-material to v9.5.49 [@&#8203;renovate](https://github.com/renovate) ([#&#8203;4733](https://github.com/mealie-recipes/mealie/issues/4733))

[1.15.2]
* Update mealie to 2.4.2
* [Full Changelog](https://github.com/mealie-recipes/mealie/releases/tag/v2.4.2)
* fix: SyntaxWarning for Escape Characters in String Literals [@&#8203;ConduciveMocha](https://github.com/ConduciveMocha) ([#&#8203;4792](https://github.com/mealie-recipes/mealie/issues/4792))
* fix: 3892 missing parameter documentation [@&#8203;VTerret](https://github.com/VTerret) ([#&#8203;4577](https://github.com/mealie-recipes/mealie/issues/4577))
* fix: Autocomplete Accessibility on Login form [@&#8203;dvdpearson](https://github.com/dvdpearson) ([#&#8203;4837](https://github.com/mealie-recipes/mealie/issues/4837))
* fix: RecipeActionMenu location [@&#8203;Kuchenpirat](https://github.com/Kuchenpirat) ([#&#8203;4835](https://github.com/mealie-recipes/mealie/issues/4835))

[1.16.0]
* Update mealie to 2.5.0
* [Full Changelog](https://github.com/mealie-recipes/mealie/releases/tag/v2.5.0)
* Migrated the "Last Made" and "On Hand" features to the household level for improved organization and usability.
* Redesignes the FAQ section in our documentation, featuring collapsible spoilers for better readability.
* feat: add make_admin script [@&#8203;Kuchenpirat](https://github.com/Kuchenpirat) ([#&#8203;4853](https://github.com/mealie-recipes/mealie/issues/4853))
* feat: Move "on hand" and "last made" to household [@&#8203;michael-genson](https://github.com/michael-genson) ([#&#8203;4616](https://github.com/mealie-recipes/mealie/issues/4616))
* feat: add delete option to three dots menu [@&#8203;johnpc](https://github.com/johnpc) ([#&#8203;4842](https://github.com/mealie-recipes/mealie/issues/4842))
* fix: Allow scraping calories as number [@&#8203;parumpum](https://github.com/parumpum) ([#&#8203;4854](https://github.com/mealie-recipes/mealie/issues/4854))
* fix: remove edit scale icon when not scalable [@&#8203;Kuchenpirat](https://github.com/Kuchenpirat) ([#&#8203;4849](https://github.com/mealie-recipes/mealie/issues/4849))
* fix: remove kitchen timer [@&#8203;Kuchenpirat](https://github.com/Kuchenpirat) ([#&#8203;4857](https://github.com/mealie-recipes/mealie/issues/4857))
* fix: Preserve orientation when converting to .webp [@&#8203;ConduciveMocha](https://github.com/ConduciveMocha) ([#&#8203;4803](https://github.com/mealie-recipes/mealie/issues/4803))
* fix: Recipe comments display a username/id rather than  [@&#8203;shethshlok](https://github.com/shethshlok) ([#&#8203;4726](https://github.com/mealie-recipes/mealie/issues/4726))
* fix: Refresh recipe section when clicking card tag chip [@&#8203;parumpum](https://github.com/parumpum) ([#&#8203;4810](https://github.com/mealie-recipes/mealie/issues/4810))
* fix: PWA - Allow CORS and add UseCredentials to nuxt.config.js [@&#8203;Borriborri](https://github.com/Borriborri) ([#&#8203;4902](https://github.com/mealie-recipes/mealie/issues/4902))
* fix: Show All Recipes in Cookbook Regardless of Sort [@&#8203;michael-genson](https://github.com/michael-genson) ([#&#8203;4908](https://github.com/mealie-recipes/mealie/issues/4908))

[1.17.0]
* Update mealie to 2.6.0
* [Full Changelog](https://github.com/mealie-recipes/mealie/releases/tag/v2.6.0)
* feat: Shopping list UI overhaul - three dot menu [@&#8203;Wetzel402](https://github.com/Wetzel402) ([#&#8203;4415](https://github.com/mealie-recipes/mealie/issues/4415))
* feat: Add Servings/Yield to Recipe Actions [@&#8203;michael-genson](https://github.com/michael-genson) ([#&#8203;4952](https://github.com/mealie-recipes/mealie/issues/4952))
* \~~feat: Add new labels and foods for en-US language and update seeding logic~~ (reverted for now) [@&#8203;Cameronwyatt](https://github.com/Cameronwyatt) ([#&#8203;4812](https://github.com/mealie-recipes/mealie/issues/4812))
* fix: Ensure bring api docs are shown (MR 4920) [@&#8203;felixschndr](https://github.com/felixschndr) ([#&#8203;4948](https://github.com/mealie-recipes/mealie/issues/4948))
* fix: Remove API Tokens from User APIs [@&#8203;michael-genson](https://github.com/michael-genson) ([#&#8203;4985](https://github.com/mealie-recipes/mealie/issues/4985))
* fix: Fixed LastMade recipes sorting order [@&#8203;PancakeZik](https://github.com/PancakeZik) ([#&#8203;4980](https://github.com/mealie-recipes/mealie/issues/4980))
* fix: revert "feat: Add new labels and foods for en-US language" [@&#8203;hay-kot](https://github.com/hay-kot) ([#&#8203;4996](https://github.com/mealie-recipes/mealie/issues/4996))
* chore(l10n): New Crowdin updates [@&#8203;hay-kot](https://github.com/hay-kot) ([#&#8203;4934](https://github.com/mealie-recipes/mealie/issues/4934))
* chore(l10n): New Crowdin updates [@&#8203;hay-kot](https://github.com/hay-kot) ([#&#8203;4938](https://github.com/mealie-recipes/mealie/issues/4938))
* chore(l10n): New Crowdin updates [@&#8203;hay-kot](https://github.com/hay-kot) ([#&#8203;4953](https://github.com/mealie-recipes/mealie/issues/4953))
* chore(auto): Update pre-commit hooks [@&#8203;github-actions](https://github.com/github-actions) ([#&#8203;4965](https://github.com/mealie-recipes/mealie/issues/4965))
* chore(l10n): New Crowdin updates [@&#8203;hay-kot](https://github.com/hay-kot) ([#&#8203;4968](https://github.com/mealie-recipes/mealie/issues/4968))

[1.18.0]
* Update mealie to 2.7.0
* [Full Changelog](https://github.com/mealie-recipes/mealie/releases/tag/v2.7.0)
* feat: support `_FILE` suffix for docker secrets (again) [@&#8203;RMI78](https://github.com/RMI78) ([#&#8203;4958](https://github.com/mealie-recipes/mealie/issues/4958))
* feat: Improve Shopping List UI [@&#8203;miah120](https://github.com/miah120) ([#&#8203;4608](https://github.com/mealie-recipes/mealie/issues/4608))
* feat: Better Scraping/More User Agents [@&#8203;michael-genson](https://github.com/michael-genson) ([#&#8203;5091](https://github.com/mealie-recipes/mealie/issues/5091))
* feat: redesign recipe info card [@&#8203;Kuchenpirat](https://github.com/Kuchenpirat) ([#&#8203;5026](https://github.com/mealie-recipes/mealie/issues/5026))
* fix: Shorten Indexes [@&#8203;michael-genson](https://github.com/michael-genson) ([#&#8203;5045](https://github.com/mealie-recipes/mealie/issues/5045))
* fix: Only run migration data fixes on migrations [@&#8203;michael-genson](https://github.com/michael-genson) ([#&#8203;5038](https://github.com/mealie-recipes/mealie/issues/5038))

[1.18.1]
* Update mealie to 2.7.1
* [Full Changelog](https://github.com/mealie-recipes/mealie/releases/tag/v2.7.1)
* fix: Remove `br` encoding from scraper [@&#8203;michael-genson](https://github.com/michael-genson) ([#&#8203;5115](https://github.com/mealie-recipes/mealie/issues/5115))
* docs(auto): Update image tag, for release v2.7.0 [@&#8203;github-actions](https://github.com/github-actions) ([#&#8203;5111](https://github.com/mealie-recipes/mealie/issues/5111))
* fix(deps): update dependency pydantic-settings to v2.8.1 [@&#8203;renovate](https://github.com/renovate) ([#&#8203;5108](https://github.com/mealie-recipes/mealie/issues/5108))
* fix(deps): update dependency openai to v1.65.1 [@&#8203;renovate](https://github.com/renovate) ([#&#8203;5123](https://github.com/mealie-recipes/mealie/issues/5123))
* chore(deps): update dependency ruff to v0.9.8 [@&#8203;renovate](https://github.com/renovate) ([#&#8203;5112](https://github.com/mealie-recipes/mealie/issues/5112))
* fix(deps): update dependency fastapi to v0.115.9 [@&#8203;renovate](https://github.com/renovate) ([#&#8203;5122](https://github.com/mealie-recipes/mealie/issues/5122))

