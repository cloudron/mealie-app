FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code

# install NODE v16 (https://github.com/mealie-recipes/mealie/blob/mealie-next/docker/Dockerfile)
ARG NODE_VERSION=16.19.1
RUN mkdir -p /usr/local/node-${NODE_VERSION} && \
    curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

## install python3.12
## https://github.com/iv-org/youtube-trusted-session-generator/blob/master/Dockerfile#L1
RUN apt-get update && \
    add-apt-repository -y ppa:deadsnakes && \
    apt-get reinstall -y --no-install-recommends python3.12 python3.12-venv python3.12-dev python3-pip python3-setuptools virtualenv virtualenvwrapper && \
    curl -sS https://bootstrap.pypa.io/get-pip.py | python3.12 && \
    update-alternatives --install /usr/bin/python python /usr/bin/python3.12 1 && \
    update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.12 1 && \
    mv /usr/local/bin/pip /usr/local/bin/pip-3.10 && \
    mv /usr/local/bin/pip3 /usr/local/bin/pip3.10 && \
    ln -s /usr/local/bin/pip3.12 /usr/local/bin/pip && \
    ln -s /usr/local/bin/pip3.12 /usr/local/bin/pip3

RUN apt-get update && \
    apt-get install --no-install-recommends -y libpq-dev libwebp-dev tesseract-ocr-all libsasl2-dev libldap2-dev libssl-dev gnupg gnupg2 gnupg1 tesseract-ocr-all && \
    rm -rf /var/lib/apt/lists/*

# renovate: datasource=github-releases depName=mealie-recipes/mealie versioning=semver extractVersion=^v(?<version>.+)$
ARG MEALIE_VERSION=2.7.1

# this will be picked up by asset building and exposed in the UI
ENV GIT_COMMIT_HASH=d639d168fa7c39ffb802d46ba05e4ec4de33faf9

RUN curl -L https://github.com/mealie-recipes/mealie/archive/refs/tags/v${MEALIE_VERSION}.tar.gz  | tar -xz --strip-components 1 -C /app/code/

# build frontend (https://github.com/docker/build-push-action/issues/471)
WORKDIR /app/code/frontend
RUN yarn install --prefer-offline --frozen-lockfile --non-interactive --production=false --network-timeout 1000000 && \
    yarn generate && \
    yarn cache clean && \
    rm -rf /usr/local/share/.cache/yarn
# end of fronend build

# build backend
ENV MEALIE_HOME="/app/code"

ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1 \
    PYSETUP_PATH="/opt/pysetup" \
    VENV_PATH="/opt/pysetup/.venv"

# prepend poetry and venv to path
ENV PATH="$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"

RUN pip install -U --no-cache-dir pip

# install poetry - respects $POETRY_VERSION & $POETRY_HOME (https://github.com/mealie-recipes/mealie/blob/mealie-next/docker/Dockerfile#L60)
ENV POETRY_VERSION=1.3.1
RUN curl -sSL https://install.python-poetry.org | python3 -

# copy project requirement files here to ensure they will be cached.
WORKDIR $PYSETUP_PATH
RUN cp /app/code/poetry.lock $PYSETUP_PATH/ && \
    cp /app/code/pyproject.toml $PYSETUP_PATH/

# install runtime deps - uses $POETRY_VIRTUALENVS_IN_PROJECT internally
RUN poetry install -E pgsql --only main

ENV PRODUCTION=true
ENV TESTING=false

# venv already has runtime deps installed we get a quicker install
WORKDIR $MEALIE_HOME
RUN source $VENV_PATH/bin/activate && poetry install -E pgsql --only main

# CRFPP Image
ENV LD_LIBRARY_PATH=/usr/local/lib
COPY --from=hkotel/crfpp:latest@sha256:6f85c2a2bf7945f54551226efe55f74baf0e7c0fb946a098c97682fded5b1192 /usr/local/lib/ /usr/local/lib
COPY --from=hkotel/crfpp:latest@sha256:6f85c2a2bf7945f54551226efe55f74baf0e7c0fb946a098c97682fded5b1192 /usr/local/bin/crf_learn /usr/local/bin/crf_learn
COPY --from=hkotel/crfpp:latest@sha256:6f85c2a2bf7945f54551226efe55f74baf0e7c0fb946a098c97682fded5b1192 /usr/local/bin/crf_test /usr/local/bin/crf_test

# Grab CRF++ Model Release
RUN python $MEALIE_HOME/mealie/scripts/install_model.py

RUN ln -sf /app/data $MEALIE_HOME/data
# end of backend build

RUN chown -R cloudron:cloudron /app/code

WORKDIR /app/code

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
