#!/bin/bash

set -eu

echo "==> Creating directories"
mkdir -p /run/mealie/.config

# migration
[[ -f /app/data/.env ]] && mv /app/data/.env /app/data/env

if [[ ! -f /app/data/env ]]; then
    echo "==> Copying default environment variables"
cat > /app/data/.env << EOF
#LDAP_USER_FILTER="cn=users,${CLOUDRON_LDAP_GROUPS_BASE_DN:-}"
#LDAP_ADMIN_FILTER="cn=admins,${CLOUDRON_LDAP_GROUPS_BASE_DN:-}"
LDAP_ADMIN_FILTER="${CLOUDRON_LDAP_USERS_BASE_DN:-}"

#
# https://nightly.mealie.io/documentation/getting-started/installation/frontend-config/
#
# Light Mode Config
THEME_LIGHT_PRIMARY=#E58325
THEME_LIGHT_ACCENT=#007A99
THEME_LIGHT_SECONDARY=#973542
THEME_LIGHT_SUCCESS=#43A047
THEME_LIGHT_INFO=#1976D2
THEME_LIGHT_WARNING=#FF6D00
THEME_LIGHT_ERROR=#EF5350

# Dark Mode Config
THEME_DARK_PRIMARY=#E58325
THEME_DARK_ACCENT=#007A99
THEME_DARK_SECONDARY=#973542
THEME_DARK_SUCCESS=#43A047
THEME_DARK_INFO=#1976D2
THEME_DARK_WARNING=#FF6D00
THEME_DARK_ERROR=#EF5350
EOF
fi

echo "==> Configuring Mealie"
cat > /run/mealie/env.sh << EOF
BASE_URL=${CLOUDRON_APP_ORIGIN}
DATA_DIR=/app/data

# Frontend
STATIC_FILES=/app/code/frontend/dist

# Backend
API_PORT=9000
GUNICORN_PORT=${API_PORT:-9000}

# Postgres
DB_ENGINE=postgres
POSTGRES_USER=${CLOUDRON_POSTGRESQL_USERNAME}
POSTGRES_PASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD}
POSTGRES_SERVER=${CLOUDRON_POSTGRESQL_HOST}
POSTGRES_PORT=${CLOUDRON_POSTGRESQL_PORT}
POSTGRES_DB=${CLOUDRON_POSTGRESQL_DATABASE}

# Email Configuration
SMTP_HOST=${CLOUDRON_MAIL_SMTP_SERVER}
SMTP_PORT=${CLOUDRON_MAIL_SMTPS_PORT}
SMTP_FROM_NAME=${CLOUDRON_MAIL_FROM}
SMTP_AUTH_STRATEGY=SSL # Options: 'TLS', 'SSL', 'NONE'
SMTP_FROM_NAME="${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Mealie}"
SMTP_FROM_EMAIL=${CLOUDRON_MAIL_FROM}
SMTP_USER=${CLOUDRON_MAIL_SMTP_USERNAME}
SMTP_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}

# LDAP (not working in v1.0.0beta-5)
LDAP_AUTH_ENABLED=true
LDAP_TLS_INSECURE=true
LDAP_ENABLE_STARTTLS=false
LDAP_SERVER_URL="${CLOUDRON_LDAP_URL:-}"
LDAP_BASE_DN="${CLOUDRON_LDAP_USERS_BASE_DN:-}"
LDAP_QUERY_BIND="${CLOUDRON_LDAP_BIND_DN:-}"
LDAP_QUERY_PASSWORD="${CLOUDRON_LDAP_BIND_PASSWORD:-}"
LDAP_ID_ATTRIBUTE=username
LDAP_NAME_ATTRIBUTE=displayname
LDAP_MAIL_ATTRIBUTE=mail
# LDAP_USER_FILTER="cn=users,${CLOUDRON_LDAP_GROUPS_BASE_DN:-}"
# LDAP_ADMIN_FILTER="cn=admins,${CLOUDRON_LDAP_GROUPS_BASE_DN:-}"

WEB_GUNICORN=true
WORKERS_PER_CORE=0.5
MAX_WORKERS=1
WEB_CONCURRENCY=1
EOF

set -o allexport
source /app/data/env
source /run/mealie/env.sh

echo "==> Database migration"

# Activate virtual environment
source /opt/pysetup/.venv/bin/activate

# Initialize Database Prerun
poetry run python /app/code/mealie/db/init_db.py


echo "==> Changing permissions"
chown -R cloudron:cloudron /run/mealie /app/data

echo "==> Starting app"
exec uvicorn mealie.app:app --host 0.0.0.0 --port $GUNICORN_PORT
